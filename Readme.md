## Magento 2 Trained Partner Program: Developer Exercises 

# Unit Two. Controllers

Create a new controller that takes orderID as a parameter and returns a json with information about order status, total, items (sku, item_id, price) and total invoiced. (Training2_OrderController)

Go to: [http://mage2.local/m2t2order/index/index/orderID/1](http://mage2.local/m2t2order/index/index/orderID/1)


Is there a better way for running this test?

```
$ ./vendor/bin/phpunit --include-path=/Users/cyrill.schumacher/Sites/magento2/dev/tests/unit/tmp/var/generation/ vendor/SchumacherFM/m2t2order/Test/
```
