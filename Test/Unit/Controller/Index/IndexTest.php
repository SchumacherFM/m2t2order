<?php

namespace SchumacherFM\M2T2Order\Test\Unit\Controller\Index;

class IndexTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \SchumacherFM\M2T2Order\Controller\Index\Index
     */
    protected $controller;


    protected function setUp()
    {

        $contextMock = $this->getMock('Magento\Backend\App\Action\Context', ['getRequest'], [], '', false);
        $requestMock = $this->getMock('Magento\Framework\App\Request\Http', ['getParam'], [], '', false);
        $requestMock->expects($this->once())->method('getParam')->willReturn('666');
        $contextMock->expects($this->once())->method('getRequest')->willReturn($requestMock);

        $orderMock = $this->getMockBuilder('Magento\Sales\Model\Order')
            ->disableOriginalConstructor()
            ->getMock();

        $orderMock->expects($this->once())->method('getItems')->willReturn([]);

        $orderRepositoryMock = $this->getMockBuilder('Magento\Sales\Api\OrderRepositoryInterface')
            ->getMockForAbstractClass();
        $orderRepositoryMock->expects($this->once())
            ->method('get')
            ->with(666)
            ->willReturn($orderMock);

        $response = [
            'status'         => null,
            'total'          => null,
            'total_invoiced' => null,
            'items'          => [],
        ];
        $resultJsonMock = $this->getMockBuilder('Magento\Framework\Controller\Result\Json')
            ->disableOriginalConstructor()
            ->getMock();
        $resultJsonMock->expects($this->once())
            ->method('setData')
            ->with($response)
            ->willReturnSelf();

        $resultJSONFactoryMock = $this->getMockBuilder('\Magento\Framework\Controller\Result\JsonFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $resultJSONFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($resultJsonMock);

        $helper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->controller = $helper->getObject(
            'SchumacherFM\M2T2Order\Controller\Index\Index',
            [
                'context'           => $contextMock,
                'salesOrder'        => $orderRepositoryMock,
                'resultJsonFactory' => $resultJSONFactoryMock,
            ]
        );
    }

    public function testExecuteResultPage()
    {
        $resultjsonMock = $this->getMockBuilder('\Magento\Framework\Controller\Result\Json')
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertSame($resultjsonMock, $this->controller->execute());
    }

}
